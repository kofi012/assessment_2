sudo apt update
sudo apt install -y haproxy
sudo sh -c "cat >>/etc/haproxy/haproxy.cfg <<_END_
frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   server afks-php 172.31.41.8:80 check
   server afks-php-s2 172.31.40.214:80 check

_END_"

sudo systemctl restart haproxy

'