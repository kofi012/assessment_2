hostname=$1

ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ec2-user@$hostname '

    sudo yum update
    sudo yum install httpd
    sudo systemctl enable httpd
    sudo systemctl start httpd

'