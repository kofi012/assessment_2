# Hello! Welcome to install_wildfly.sh
# Pre requesites:
#   - have an ubuntu 20.04 machine
#   - have a Jenkins set up (this set up is in the repo)
#   - ensure the private ip for the ubuntu instance is used when configuring the build setup as instructed in jenkins.md
#   - If you're trying to recreate this make sure you download latest version of wildfly (line 18)

# update all packages
sudo apt update
 
# installs java 
sudo apt install default-jdk -y

# open link and download TGZ https://www.wildfly.org/downloads/
# or

# downloads wildfly from github
wget https://github.com/wildfly/wildfly/releases/download/26.0.1.Final/wildfly-26.0.1.Final.tar.gz

# unzips wildfly download
tar -xf wildfly-*.Final.tar.gz

# moves wildfly to /opt location
sudo mv wildfly-*Final /opt/wildfly

# To run the WildFly with a non-root user without sudo access, we create a separate user that will have only access to its files and folders.
# Add Group

sudo groupadd -r wildfly

# Add a new user
sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly

# Change Ownership of the fly
sudo chown -RH wildfly:wildfly /opt/wildfly

# create wildfly directory under /etc and copy some files to pre-configure service to run wildfly
sudo mkdir -p /etc/wildfly

sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/

sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/

sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/

# make all .sh scripts executable
sudo chmod +x /opt/wildfly/bin/*.sh

# To make the WildFly application server run automatically with system boot; start and enable its service
sudo systemctl enable --now wildfly

sudo systemctl daemon-reload

sudo systemctl restart wildfly

## nginx

# update all packages
sudo apt update

# installs nginx
sudo apt install nginx -y 

## Setup Nginx as a Reverse proxy for WildFly

# changing config setting of nginx
sudo sh -c 'echo "proxy_set_header Host \$host;
proxy_set_header X-Forwarded-Proto \$scheme;
add_header Front-End-Https on;
add_header Cache-Control no-cache;" > /etc/nginx/conf.d/proxy_headers.conf'

# allow from any port to view wildfly
sudo sh -c 'echo "server {
  listen          80;
  server_name     SERVER-IP;

  location / {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:8080;
  }

  location /management {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990/management;
  }

  location /console {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990/console;
  }

  location /logout {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990/logout;
  }

  location /error {
    include conf.d/proxy_headers.conf;
    proxy_pass http://127.0.0.1:9990;
  }

}" > /etc/nginx/sites-available/default'

# remove prexisting symlink
sudo rm /etc/nginx/sites-enabled/default

# create symlink 
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/

# checks nginx config 
sudo nginx -t

# restart nginx
sudo systemctl restart nginx