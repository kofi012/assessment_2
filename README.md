# Assessment 2 - Group B - AFKS

In this repository you will find instructions on how to do the following:

- Install a HA_Proxy on an <server> machine in AWS
- Install Wildfly and nginx on a ubuntu machine and listening on port '80'
- Install Jenkins on a ubuntu machine in AWS, connect to Bitbucket and create CI/CD pipelines with Slack connected to receive notifications.


## Pre requisities

- AWS Account with key pair.
- Bitbucket repository.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#### Jenkins

I have added a seperate markdown file called *jenkins.md* which has instructions on how to install Jenkins, how to create User and how we have created and triggered our CICD Pipeplines.

For the above information please check out [Jenkins.md](https://bitbucket.org/kofi012/assessment_2/src/master/Jenkins.md)
