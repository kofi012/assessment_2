<?php

/**
 * Open a connection via PDO to create a
 * new database and table with structure.
 *
 */

require "config.php";

// Create the init.sql file using our variables
$initsql=fopen("data/init.sql","w");
fwrite($initsql,"use $dbname;\n");
$table="CREATE TABLE users (
	id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	firstname VARCHAR(30) NOT NULL,
	lastname VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
	age INT(3),
	location VARCHAR(50),
	date TIMESTAMP
);";
fwrite($initsql,$table);
fclose($initsql);

try {

$pdo = new PDO($dsn, $username, $password);

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = file_get_contents("data/init.sql");

$pdo->exec($sql);
echo "successfully connected"; 
}
catch(PDOException $e)
{
t;    
echo "Connection failed: <br>  ". $e->getMessage(); 

}

$conn = null;

?>
