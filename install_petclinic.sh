# # prompts user to enter ip
# read -p "Please enter IP address: " 

# # if statement used to check if user has input ip then outputs welcome message, if not then re-prompt
# if [ -z "$REPLY" ]
#     then
#         echo "Please enter an IP address"
#     else
#         echo "Welcome $REPLY"
# fi

# HOSTNAME=$1
# KEY='~/.ssh/ch9_shared.pem'
# ssh -o StrictHostKeyChecking=no -i $KEY ubuntu@$HOSTNAME 
    
#installs maven
sudo apt update 
sudo apt install maven -y

#installs jdk
sudo apt update -y
sudo apt install default-jdk -y

#clones petclinic repo and runs it
git clone https://github.com/spring-projects/spring-petclinic.git
cd spring-petclinic
./mvnw spring-boot:run 

# sudo sh -c "cat >> src/main/resources/application.properties <<_END_
# database=petclinic
# spring.datasource.url=jdbc:mysql://afks-petclinic-server.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com3306/petclinic
# spring.datasource.username=admin
# spring.datasource.password=arslan00
# "
#mvn -Dmaven.test.skip=true package