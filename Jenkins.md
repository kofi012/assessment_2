# How to Install Jenkins on a Ubuntu machine, connect to Bitbucket, create CI/CD pipelines and send notifications to Slack.

### Pre requisities

- An Ubuntu machine running in AWS. 
- Key pair you used to create your ubuntu machine.

-----------------------------------------------------------------------------------------------------------

### Step 1: Installing Jenkins on Ubuntu Machine

In order to have Jenkins installed, you have two options:

1. Clone this repository to your local machine so you have access to our `install_jenkins.sh` file (chmod has already been done)
2. Create your own bash script in a local repository and copy the commands from `install_jenkins.sh` 
    a. to make sure you can run your script, use the `chmod +x <filename>` to give permissions to run.
    b. you will also need to change key variable on script to the `path of your key`

### Step 2: Running the script to install Jenkins on the Ubuntu machine in AWS.

Before you follow the below commands, make sure you are in the `path to your local repository`

```bash

# To run script on your virtual machine using it's public IP
$  <yourscript.sh> <PublicIPaddress>

# Example
$ ./install_jenkins.sh 54.155.92.80

```

### Step 3: Check Jenkins is on port 8080 and create an account.

Open new browser tab and go to `http://<IPaddress>:8080`

You shall see the following:

![Unlock-Jenkins](./images/unlock-jenkins.jpeg)

Once you have the above showing, you will need to manually ssh into your virtual machine.

```bash 
ssh -i ~/.ssh/<sshkey> ubuntu@<ipaddress>
```

navagate to path `/var/lib/jenkins` and use the following command

```bash
cat /var/lib/jenkins/secrets/initialAdminPassword
```

Copy the output into the **Administrator Password** on your Jenkins web browser.
 - You will need to select *Install suggested plugins*

 Once plugins all installed, you will then need to create a login.

 Then you should receive a page with IP address for Jenkins.

 #### To start creating your pipelines...

 ![start-Pipeline](./images/start-pipelines.jpeg)

 ## How to use our Jenkins

Pre requisities

- Make sure the instance is running in AWS
- Navigate to to instances Public IP address:8080


#### Log in Details:

```bash

Username: afks
Password: cohort-9

```
Once logged in you will see something similar to the following:

![Dashboard](./images/dashboard.jpeg)


Here you can see all of our CI/CD pipelines. Each of them have their own triggers. 

#### CI Pipelines

> Each CI Pipeline is triggered/built from a single push to our repository. In order for the build to only push what is relevant to be changed, you will need to specify a branch manager



![buildtrigger](./images/build-triggers.jpeg)



#### CD Pipelines 

> Each CD Pipeline is being triggered once CI pipeline build is successful. All builds are being sent to SLACK to notify of build.

![CDTriggers](./images/CDTrigger.jpeg)


### These are just the simple steps on how we created Jenkins and built our pipelines, if you need anymore information or feel we have missed any steps, please let us know.

















