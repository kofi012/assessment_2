# Wildfly set up

# Pre requesites

  - Have an ubuntu 20.04 machine
  - Have a Jenkins set up (this set up is in the repo)
  - Ensure the private ip for the ubuntu instance is used when configuring the build setup as instructed in Jenkins.md
  - If you're trying to recreate this make sure you download latest version of wildfly

# Launch steps

  - Push to the repo to trigger your pipeline
      - When triggering wildfly ensure you are in the wildfly branch
  - Go to the site page on the web browser
    - You can http://<server-ip>
    - Or http://afks-wildfly.academy.labs.automationlogic.com


# Changing the DNS

  - On AWS go to route 53
  - Under DNS management click on hosted zone
  - Select academy.labs.automationlogic.com
  - Click on afks-wildfly.academy.labs.automationlogic.com and edit record
  - Under 'Record Name' you can change the name
  - Click save then go to your site.

This has now been set up and you can http://afks-wildfly.academy.labs.automationlogic.com