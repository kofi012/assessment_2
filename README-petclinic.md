# Petclinic Setup

This file will take you through step by step on how to setup your Petclinic site and connect it to a database.

## Step 1

Fortunatley there is a script that will install everything for you.

Simply run 'install_petclinic.sh' on your ubuntu machine.

## Step 2

Once the script has correctly run you should be able to see petclinic on your browser using <machines-IPv4>:8080

Now to connect it to the RDS that hosts the database you must manually edit the files on the machine.

1. Start by stopping Petclinic by doing 'ctrl + c'

2. Change directory by doing 'cd sprint-petclinic/src/main/resources' 

3. Once you are in resources do the command 'sudo nano application.properties'

4. Edit the file by adding the following text 

``` 
database=petclinic
spring.datasource.url=jdbc:mysql://afks-petclinic-server.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com:3306/petclinic
spring.datasource.username=petclinic
spring.datasource.password=petclinic

```
(this is specfic text for this RDS connection change the following text for your own RDS)

```
database=petclinic
spring.datasource.url=jdbc:mysql://<RDS-endpoint>:3306/petclinic
spring.datasource.username=<username>
spring.datasource.password=<password>

```

5. Now to recompile the project, change the directory back to spring-petclinic and run the following command 'mvn -Dmaven.test.skip=true package'

6. If you have no build errors you have successfully connected the RDS and now use the command './mvnw spring-boot:run' to launch the petclinic site.

## Congrats you have no successfully launched petclinic.



